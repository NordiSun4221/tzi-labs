using System.ComponentModel.DataAnnotations;

namespace tzi.Models
{
    public class LoginModel
    {
        [Required]
        [EmailAddress]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }
        
        [Required] 
        public string Algorithm { get; set; }
    }
}