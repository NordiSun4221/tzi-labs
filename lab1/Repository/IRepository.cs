using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using tzi.Models;

namespace tzi.Repository
{
    public interface IRepository
    {
        Task<IEnumerable<LoginData>> GetAll();

        Task<int> Add(LoginData data);
    }
}