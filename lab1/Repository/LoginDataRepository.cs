using Dapper;
using System;
using tzi.Models;
using System.Data;
using Microsoft.Data.Sqlite;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Options;

namespace tzi.Repository
{
    public class LoginDataRepository : IRepository, IDisposable
    {
        private readonly IDbConnection _connection;

        public LoginDataRepository(IOptions<DbConnection> options)
        {
            _connection = new SqliteConnection(options.Value.Connection);
            _connection.Open();
        }
        
        public async Task<IEnumerable<LoginData>> GetAll()
        {
            return await _connection.QueryAsync<LoginData>("select * from loginData;");
        }

        public async Task<int> Add(LoginData data)
        {
            var query = "insert into loginData(login, password) values(@Login, @Password)";

            return await _connection.ExecuteAsync(query, data);
        }

        public void Dispose()
        {
            _connection.Close();
            _connection.Dispose();
        }
    }
}