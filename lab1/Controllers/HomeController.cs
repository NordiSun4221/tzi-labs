﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tzi.Models;
using tzi.Repository;
using tzi.Services;

namespace tzi.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRepository _repository;
        
        public HomeController(IRepository repository)
        {
            _repository = repository;
        }
        
        public async Task<IActionResult> Index()
        {
            return View(await _repository.GetAll());
        }

        public IActionResult AddLoginData()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Add(LoginModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("AddLoginData", model);
            }

            var password = "";

            if (model.Algorithm == "sha256")
            {
                Console.WriteLine("SHA256");
                password = Sha256.HashString(model.Password);
            }
            else if (model.Algorithm == "md5")
            {
                Console.WriteLine("MD5");
                password = Md5.Hash(model.Password);
            }

            await _repository.Add(new LoginData
            {
                Login = model.Login, 
                Password = password
            });
            
            return RedirectToAction(nameof(Index));
        }

    }
}
